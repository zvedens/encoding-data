/**
 * This file updates our datasets for our chart-js chart.
 *
 * Created by Zechariah Edens on 03/15/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/21/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */
import timeRange from "./timeRange";

export const barColor = [
  "#155d27",
  "#1a7431",
  "#208b3a",
  "#25a244",
  "#2dc653",
  "#4ad66d",
  "#6ede8a",
  "#92e6a7",
  "#b7efc5",
];

export const channelColors = [
  "#264653",
  "#17c3b2",
  "#ffcb77",
  "#fe6d73",
  "#c77dff",
];

/**
 * A helper function that allows you to quick create data objects
 * for the Chart.js data set with default colors.
 * @param data
 * @param label
 * @param backgroundColor
 * @param borderColor
 * @param color
 * @returns {{backgroundColor: string, borderColor: string, color: string, data, label}}
 */
export const chartJsDataObj = (
  data = {},
  label,
  backgroundColor = "rgb(255, 99, 132)",
  borderColor = "rgb(0, 110, 110)",
  color = "black"
) => {
  return {
    label,
    backgroundColor,
    borderColor,
    color,
    data,
  };
};

/**
 * This function generates the dataset needed for single channel to be displayed.
 * @param {string[]} channel
 * @param {string} date - if specified, the data given back will be for this specific date
 * @returns {counts: object, label: string, date: string}
 */
export const generateChannelDatasetsTotal = (
  channels,
  date = "",
  channelData
) => {
  const returnArr = [];

  console.log(channels);

  for (const channel of channelData) {
    let counts = { ...timeRange };
    let label = channel.channel;
    if (channels.includes(channel.channel)) {
      for (const key in channel) {
        if (key === channel) {
          label = key;
          continue; // skip to next iteration
        }

        const filterDate = new Date(date);
        const filterDateStr = `${
          filterDate.getMonth() + 1
        }/${filterDate.getDate()}/${filterDate.getFullYear()}`;

        if (date && key !== filterDateStr) continue;
        for (const time in channel[key]) {
          if (time.length < 5) continue;
          counts[time] += channel[key][time];
        }
      }

      returnArr.push({ counts, label, date });
    }
  }

  return returnArr;
};

/**
 * This function is used when only the date is given. Otherwise use generateChannelDatasetsTotal
 * @param dayData
 * @param filterDate
 * @returns {object[]}
 */
export const generateByDateDataset = (dayData, filterDate = "") => {
  let datasets = [];
  let i = 0;
  for (let date in dayData) {
    if (filterDate && filterDate !== date) continue;
    const data = { ...dayData[date] };
    delete data.day;

    datasets.push({
      label: `${dayData[date].day} - ${date}`,
      backgroundColor: barColor[i],
      borderColor: "rgb(0,110,110)",
      color: "black",
      data: data,
    });
    i++;
  }
  return datasets;
};

export const generateChannelDataset = (channelData) => {
  // go through every object that doesn't have value of 'channel'

  let datasets = [];
  let i = 0;

  for (const date in channelData) {
    const data = { ...channelData[date] };
    if (date !== "channel") {
      datasets.push({
        label: channelData[date],
        backgroundColor: barColor[i],
        borderColor: "rgb(0,110,110)",
        color: "black",
        data: data,
      });
    }
    if (i >= 8) {
      i === 0;
    } else {
      i++;
    }
  }
  return datasets;
};
