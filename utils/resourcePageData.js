export const resourcePageData = [
  {
    header: "Repository",
    paragraph:
      "Our repository consists of a full stack application. Our backend features an express nodeJs application that primarily deals with API calls, and reducing that data to exactly what our frontend requires. Our frontend is a React NextJs application using Bootstrap styling, which shows the data, including filters.",
    link: "https://gitlab.com/zvedens/encoding-data",
  },
  {
    header: "Architectural Diagram",
    paragraph:
      "Our Architectural Diagram goes over our API call strategy, data manipulation, and will include future iterations.",
    link: "https://lucid.app/lucidchart/fb880683-d173-4f27-83d4-4a57732f2c8f/edit?invitationId=inv_1a070d11-3a8b-476a-9b58-f3330924062a&referringApp=slack&page=0_0#?folder_id=shared",
  },
  {
    header: "Wiki Page",
    paragraph:
      "Our Wiki page contains a longer, in depth readme about our project. We go over use cases, future implementation, links to diagrams, etc.",
    link: "https://gitlab.com/zvedens/encoding-data",
  },
];
