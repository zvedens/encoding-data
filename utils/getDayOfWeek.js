/**
 * This file returns an array of the days of the week, with a second for the current day
 *
 * Created by Zechariah Edens on 03/15/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/21/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

export const getDayOfWeek = (date) => {
  const day = new Date(date).getDay();
  return [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ][day];
};
