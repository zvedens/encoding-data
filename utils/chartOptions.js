export const chartOptions = {
  responsive: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: false,
      text: "Chart.js Bar Chart",
    }
  },
  scales: {
    x: {
      title: {
        display: true,
        text: 'Time (UTC)'
      }
    },
    y: {
      title: {
        display: true,
        text: 'Programs Transcoding'
      }
    }
  },
};
