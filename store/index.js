/**
 * This file componentizes our Dropdown logic for both Date and Channel.
 *
 * Created by Zechariah Edens and Taanileka Maama on 03/21/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/22/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import { useContext, createContext, useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  chartJsDataObj,
  generateChannelDatasetsTotal,
  generateByDateDataset,
  channelColors,
} from "../utils/datasets";
import timeRange from "../utils/timeRange";
import { colorPink, colorGreen } from "../styles/colors.module.scss";

// Define a new context
const transcodingContext = createContext();

// Deconstruct the provider for ease of use in JSX
const { Provider } = transcodingContext;

export const useMainContext = () => {
  return useContext(transcodingContext);
};

/**
 * Context provider component.
 * @param {object} props
 */
export const TranscodingProvider = ({ children }) => {
  const initialFilterSettings = {
    date: "",
    channels: [],
    viewTotals: true,
  };
  const [showGraph, setShowGraph] = useState(false);
  const [showResourcePage, setShowResourcePage] = useState(false);
  const [channelData, setChannelData] = useState([]);
  const [channelByDateData, setChannelByDateData] = useState([]);
  const [weekData, setWeekData] = useState({});
  // const dayDataFinal = generateDayDataset(channelByDateData);
  const [isLoading, setIsLoading] = useState(false);
  const [filters, setFilters] = useState(initialFilterSettings);
  const labels = Object.keys(timeRange);
  const [graphData, setGraphData] = useState({
    labels,
    datasets: [chartJsDataObj([weekData], "TotalCounts")],
  });
  const [sheetData, setSheetData] = useState();
  const [color, setColor] = useState(colorPink);
  const [csvData, setCsvData] = useState();

  useEffect(() => {
    setIsLoading(true);
    fetch("http://localhost:8080/api/content")
      .then((res) => res.json())
      .then((body) => {
        setChannelData(body.byChannel);
        setChannelByDateData(body.byDate);
        setWeekData(body.total);
        setIsLoading(false);
      })
      .catch((err) => console.error(err));
  }, []);

  useEffect(() => {
    getCSVData();
  }, [filters]);

  useEffect(() => {
    if (!filters.channels.length && !filters.viewTotals) {
      const datasets = generateByDateDataset(channelByDateData, filters.date);
      setGraphData({
        ...graphData,
        datasets,
      });
    } else if (filters.channels.length) {
      const selectedChannelsList = generateChannelDatasetsTotal(
        filters.channels,
        filters.date,
        channelData
      );

      const chartJsList = selectedChannelsList.map(
        ({ counts, label, date }, index) =>
          chartJsDataObj(
            counts,
            `${label}${date ? " " + date : ""}`,
            channelColors[index]
          )
      );

      setGraphData({
        ...graphData,
        datasets: chartJsList,
      });
    } else if (filters.viewTotals) {
      setGraphData({
        ...graphData,
        datasets: [chartJsDataObj(weekData, "Total Counts")],
      });
    }
  }, [filters]);

  useEffect(() => {
    filters.viewTotals ? setColor(colorPink) : setColor(colorGreen);
  }, [filters]);

  /**
   * Updates dropdown filters.
   * @param {object} props
   */
  const handleFilterChange = (filterKey, filterValue) => {
    let newChannelsFilter;
    if (filterKey === "channels" && filterValue !== "") {
      filters.channels.includes(filterValue)
        ? (newChannelsFilter = filters.channels.filter(
            (channel) => channel !== filterValue
          ))
        : (newChannelsFilter = [...filters.channels, filterValue]);

      if (newChannelsFilter.length > 5) return; // Don't allow more then 5 channels filtered.
    } else if (filterKey === "channels" && filterValue === "")
      return setFilters(initialFilterSettings);

    const newFilters = {
      ...filters,
      [filterKey]: newChannelsFilter || filterValue,
    };

    if (filterKey !== "viewTotals") {
      newFilters.viewTotals = false;
    } else {
      newFilters.channels = [];
      newFilters.date = "";
    }

    setFilters(newFilters);
  };

  /**
   * @returns channel name
   */
  const getChannelNames = () => {
    return channelData
      .map((channel) => {
        return channel.channel;
      })
      .sort();
  };

  /**
   *
   * @returns CSV data, which exports as an Array of Arrays, where Each Array is a line for the CSV file.
   */
  const getCSVData = () => {
    // our final array is initialized
    const data = [];
    /**
     * header: First three items in our title Row
     * times: total times in 30min increments for our title Row
     */
    // total times in 30min increments
    const header = ["Channel Name", "Dates"];
    const times = Object.keys(timeRange);
    times.map((time) => header.push(time));
    data.push(header), data.push([]);

    /**
     * FOR EACH CHANNEL OBJECT
     * channelLineGroup: This will be 7 number of lines for each date. First Line Column 1 will be the channel Name, Column 2 will be date, and then data for each time increment.
     * Every subsequent line (6 more) will be empty on column 1, date on column 2, and then repeat.
     */

    for (const channelObject of channelData) {
      let i = 0;
      for (const [key, value] of Object.entries(channelObject)) {
        const currentChannel = [];
        const currentDate = key;
        const currentValueObject = value;
        const arrayOfValueData = [];
        for (const [subKey, subValue] of Object.entries(currentValueObject)) {
          arrayOfValueData.push(subValue);
        }
        // console.log(currentDate);
        // console.log(currentValueObject);
        currentDate === "channel" && currentChannel.push(currentValueObject);
        currentDate !== "channel" &&
          currentChannel.push("", currentDate, ...arrayOfValueData);
        data.push(currentChannel);
      }
    }
    // console.log(data);
    setSheetData(data);
    let scvContent =
      "data:text/csv;charset=utf-8," + data.map((e) => e.join(",")).join("\n");
    setCsvData(scvContent);
    return;
  };

  const changePage = (type) => {
    if (type === "table") {
      if (showGraph) {
        setShowGraph(false), setShowResourcePage(false);
      } else {
        setShowGraph(true), setShowResourcePage(false);
      }
    }
    if (type === "info") {
      if (showResourcePage) {
        setShowResourcePage(false), setShowGraph(false);
      } else {
        setShowResourcePage(true), setShowGraph(false);
      }
    }
  };

  // Take an object who's title becomes the first item in the array, and each value inside the array as each subsequent items in the array

  return (
    <Provider
      value={{
        channelData,
        setChannelData,
        channelByDateData,
        setChannelByDateData,
        weekData,
        setWeekData,
        isLoading,
        setIsLoading,
        graphData,
        setGraphData,
        handleFilterChange,
        getChannelNames,
        filters,
        color,
        setColor,
        csvData,
        changePage,
        showGraph,
        sheetData,
        showResourcePage,
      }}
    >
      {children}
    </Provider>
  );
};

TranscodingProvider.propTypes = {
  children: PropTypes.node,
};
