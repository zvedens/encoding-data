/**
 * Root file for our React project, including our global state from the store.
 *
 * Created by Zechariah Edens on 03/15/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/21/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import { TranscodingProvider } from "../store";

function MyApp({ Component, pageProps }) {
  return (
    <TranscodingProvider>
      <Component {...pageProps} />
    </TranscodingProvider>
  );
}

export default MyApp;
