# Transcoding Data

## Table of Contents

1. [Description](#description)
2. [Getting Started](#getting-started)
3. [Usage](#usage)
4. [Links](#links)
5. [Support](#support)

## Description

- Full-stack application, currently only hosted locally.
- This application serves to help Doug Bay in sorting through consistent data of all channels transcoding information.
- This data consists of All Channels transcoding, in 30 minute increments per day, 7 days per week.
- This data can be sorted to view by Channel, by Day, by Week, or a mix of the three.
- The backend Node.js server acts as a proxy between the Next.js frontend and SBG's V2 APIs.
  - Since we are only using this application locally the data for the day is stored in Redis and refreshed when the user launches the app on a new day.
  - Side note: This data is better off stored in a data warehouse like Amazon Redshift but for the small use case Redis works.
  - Side note: Amount of request for QA for the call to get initial data from SMP API is 252. This is based on calls to v2/api/content and v2/api/programs. Once this data is received and structured for the frontend it's stored in Redis till end of day UTC time so the initial 252 calls is the only ones needed from Sinclair.
    - Initial Call is about 7 seconds because of the iterations through all the channels and programs. After the cache it runs at about 50ms.

## Installation

- (assuming we have this live in 1 repository, with an external README and package.json)
- our External package.json file will include a script for installing in both Client and Server folders.
- Our External package.json file will include a scrpt for running both Client and Server applications.
- Client Application is an 'npm run dev'
- Server Application is a 'npm start' or to use Nodemon 'npm run dev'
- Ensure you have Redis installed and it's running. For [Mac](https://developer.redis.com/create/homebrew/) and for [Windows](https://developer.redis.com/create/windows/).

## Usage

- To use this application, we will need to have both Client and Server running.
- Initial load is rather small, and Client should receive the fetch from the Server in under a second.
- Simply sort data how you'd like based on dropdowns and buttons.
- Date and Channel Indicators above bar are also able to be used to filter out data.

## Support

- zvedens@sbgtv.com
- tstmaama@sbgtv.com

## Links

- [Front-end Repository](https://gitlab.com/zvedens/encoding-data)
- [Back-end Repository](https://gitlab.com/taanibravo/transcode-backend)
- [Architectural Diagram](https://lucid.app/lucidchart/fb880683-d173-4f27-83d4-4a57732f2c8f/edit?invitationId=inv_1a070d11-3a8b-476a-9b58-f3330924062a&referringApp=slack&page=0_0#?folder_id=shared)
- [Wiki](sorry-this-doesn't-exist-quite-yet)

## Authors and acknowledgment

- Zechariah Edens - zvedens@sbgtv.com
- Taani Maama - tstmaama@sbgtv.com
