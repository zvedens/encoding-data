/**
 * This file contains our documentation page
 *
 * Created by Zechariah Edens on 03/28/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/31/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import React from "react";
import { CardGroup } from "react-bootstrap";
import ResourceCard from "./ResourceCard";
import { resourcePageData } from "../utils/resourcePageData";

const ResourcePage = () => {
  return (
    <CardGroup
      style={{
        borderRadius: "15px",
      }}
    >
      {resourcePageData?.map(({ header, paragraph, link }) => {
        return (
          <ResourceCard
            key={header}
            header={header}
            paragraph={paragraph}
            link={link}
          />
        );
      })}
    </CardGroup>
  );
};

export default ResourcePage;
