/**
 * This file contains a simple card to display documentation.
 *
 * Created by Zechariah Edens on 04/01/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 04/01/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import PropTypes from "prop-types";
import { Card } from "react-bootstrap";
import styles from "../styles/Resources.module.scss";

const ResourceCard = ({ header, paragraph, link }) => {
  const handleClick = () => {
    window.open(link);
  };
  return (
    <Card
      border="light"
      style={{
        borderRadius: "15px 0px 0px 15px",
        margin: "5px",
      }}
    >
      <Card.Header className={styles.cardHeader} onClick={handleClick}>
        {header}
      </Card.Header>
      <Card.Body>
        <Card.Text>{paragraph}</Card.Text>
      </Card.Body>
    </Card>
  );
};

ResourceCard.propTypes = {
  header: PropTypes.string,
  paragraph: PropTypes.string,
  link: PropTypes.string,
};

export default ResourceCard;
