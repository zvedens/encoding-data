/**
 * This file componentizes our Dropdown logic for both Date and Channel.
 *
 * Created by Zechariah Edens on 03/17/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 04/01/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import React from "react";
import { useMainContext } from "../store";
import styles from "../styles/Home.module.scss";

const Footer = () => {
  const { color } = useMainContext();
  return (
    <footer className={styles.footer}>
      <h5>
        Conjured by{" "}
        <span
          style={{ color: `${color}`, transition: "all 0.2s ease-out" }}
          className={styles.name}
        >
          {" "}
          Zac
        </span>{" "}
        and
        <span
          style={{ color: `${color}`, transition: "all 0.2s ease-out" }}
          className={styles.name}
        >
          Taani
        </span>
      </h5>
    </footer>
  );
};

export default Footer;
