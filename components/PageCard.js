/**
 * This file contains our main page card, which contains either our vertical chart, or our csv view
 *
 * Created by Zechariah Edens on 03/15/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/31/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import React, { useEffect, useState } from "react";
import styles from "../styles/Home.module.scss";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { useMainContext } from "../store";
import { chartOptions } from "../utils/chartOptions";
import Table from "./Table";
import ResourcePage from "./ResourcePage";

// ChartJS boilerplate
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export const PageCard = () => {
  const { graphData, handleFilterChange, showGraph, showResourcePage } =
    useMainContext();

  useEffect(() => {
    handleFilterChange("viewTotals", true);
  }, []);

  return (
    <div className={styles.body}>
      {!showGraph && !showResourcePage && (
        <div className={styles.grid}>
          <Bar
            className={styles.graph}
            options={chartOptions}
            data={graphData}
          />
        </div>
      )}
      {showGraph && !showResourcePage && (
        <div className={styles.wrapperContainer}>
          <Table />
        </div>
      )}
      {showResourcePage && !showGraph && (
        <div className={styles.wrapperContainer}>
          <ResourcePage />
        </div>
      )}
    </div>
  );
};
