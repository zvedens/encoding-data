/**
 * This file contains our data fetch, and stores in global data.
 *
 * Created by Zechariah Edens on 03/27/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 04/01/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import PropTypes from "prop-types";
import { useMainContext } from "../store";
import styles from "/styles/Table.module.scss";
import TableRow from "./TableRow";

const Table = () => {
  const { sheetData: data } = useMainContext();

  return (
    <table className={styles.table}>
      <thead className={styles.tableHeader}>
        <tr className={styles.tableRow}>
          {data[0].map((item, index) => {
            return (
              <th className={styles.th} key={index}>
                {item}
              </th>
            );
          })}
        </tr>
      </thead>
      <tbody>
        {data?.slice(2, data.length).map((item, index) => {
          return <TableRow key={index} tbRow={item} index={index} />;
        })}
      </tbody>
    </table>
  );
};

Table.propTypes = {
  data: PropTypes.array,
};

export default Table;
