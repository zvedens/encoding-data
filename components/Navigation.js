/**
 * This file contains Nav bar, with state to describe our current page
 *
 * Created by Zechariah Edens on 04/01/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 04/01/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import React, { useEffect, useState } from "react";
import { useMainContext } from "../store";
import { NavDropdown } from "./NavDropdown";
import styles from "../styles/Nav.module.scss";

const Navigation = () => {
  const [tableButtonState, setTableButtonState] = useState("Table");
  const [infoButtonState, setInfoButtonState] = useState("Resources");

  const {
    color,
    showGraph,
    filters,
    handleFilterChange,
    changePage,
    csvData,
    showResourcePage,
  } = useMainContext();

  useEffect(() => {
    showGraph
      ? setTableButtonState("Return to main page")
      : setTableButtonState("Table");
  }, [showGraph]);
  useEffect(() => {
    showResourcePage
      ? setInfoButtonState("Home Page")
      : setInfoButtonState("Resources");
  }, [showResourcePage]);

  useEffect(() => {
    handleFilterChange("viewTotals", true);
  }, []);
  return (
    <div
      className={styles.navigation}
      variant="tabs"
      style={{
        borderBottom: `5px solid ${color}`,
        transition: "all 0.2s ease-out",
      }}
    >
      <div className={styles.navItem}>
        <a
          className={`${styles.navLink} ${styles.buttonShift}`}
          onClick={() => changePage("info")}
        >
          {infoButtonState}
        </a>
      </div>
      {!showGraph && !showResourcePage && (
        <>
          <div className={styles.navItem}>
            <a className={styles.navLink}>
              <NavDropdown data={"date"} />
            </a>
          </div>
          <div className={styles.navItem}>
            <a className={styles.navLink}>
              <NavDropdown data={"channels"} />
            </a>
          </div>
          <div className={styles.navItem}>
            <a
              className={`${styles.navLink} ${styles.buttonShift}`}
              // eventKey="link-2"
              onClick={() =>
                handleFilterChange("viewTotals", !filters.viewTotals)
              }
            >
              {filters.viewTotals ? "View Counts by Day" : "View Total Counts"}
            </a>
          </div>
          <div className={styles.navItem}>
            <a
              className={`${styles.navLink} ${styles.buttonShift}`}
              onClick={() => changePage("table")}
            >
              {tableButtonState}
            </a>
          </div>
        </>
      )}
      {showGraph && !showResourcePage && (
        <>
          <div className={styles.navItem}>
            <a
              className={`${styles.navLink} ${styles.buttonShift}`}
              href={csvData}
              download="sinclair-transcoding"
              target="_blank"
              rel="noreferrer"
            >
              Download
            </a>
          </div>
          <div className={styles.navItem}>
            <a
              className={`${styles.navLink} ${styles.buttonShift}`}
              onClick={() => changePage("table")}
            >
              {tableButtonState}
            </a>
          </div>
        </>
      )}
    </div>
  );
};

export default Navigation;
