/**
 * This file contains the individual table row in our csv view.
 *
 * Created by Zechariah Edens on 03/28/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 04/01/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import PropTypes from "prop-types";
import { useMainContext } from "../store";
import styles from "../styles/Table.module.scss";

const TableRow = ({ tbRow, index }) => {
  const { color } = useMainContext();

  return (
    <tr className={styles.tableRow} key={index}>
      {tbRow.map((item, inx) => {
        return (
          <td
            className={styles.th}
            style={{ backgroundColor: item > 0 ? `${color}` : "" }}
            key={inx}
          >
            {item}
          </td>
        );
      })}
    </tr>
  );
};

TableRow.propTypes = {
  tbRow: PropTypes.array,
  index: PropTypes.number,
};

export default TableRow;
