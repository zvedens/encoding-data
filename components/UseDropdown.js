/**
 * This file componentizes our Dropdown logic for both Date and Channel.
 *
 * Created by Zechariah Edens on 03/22/2022
 * Last updated by Zechariah Edens and Taanileka Maama on 03/32/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */

import { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { useMainContext } from "../store";
import styles from "../styles/Nav.module.scss";
import PropTypes from "prop-types";

/**
 *
 * @param {string} data
 * @returns component
 */
const UseDropdown = ({ data }) => {
  const [state, setState] = useState({
    filter: "channels",
    toggle: "Filter Channels",
    title: "All Channels",
    className: styles.dropdownMenu,
    dropdown: "allChannels",
  });
  const { channelData, getChannelNames, handleFilterChange } = useMainContext();
  const channelNames = getChannelNames();

  const availableDates = [];
  for (const channel of channelData) {
    for (const channelKey in channel) {
      if (channelKey !== "channel" && !availableDates.includes(channelKey))
        availableDates.push(channelKey);
    }
  }

  useEffect(() => {
    if (data === "channels") {
      setState({
        filter: "channels",
        toggle: "Filter Channels",
        title: "All Channels",
        className: styles.dropdownMenu,
        dropdown: "allChannels",
      });
    }
    if (data === "date") {
      setState({
        filter: "date",
        toggle: "Choose Date",
        title: "All Dates",
        className: "",
        dropdown: "allDates",
      });
    }
  }, []);

  return (
    <Dropdown
      onSelect={(eventKey) => {
        handleFilterChange(`${state.filter}`, eventKey);
      }}
    >
      <Dropdown.Toggle id="dropdown-button-dark" variant="secondary">
        {state.toggle}
      </Dropdown.Toggle>

      <Dropdown.Menu className={state.className} variant="dark">
        <Dropdown.Item key={state.dropdown} eventKey={""} value={""}>
          {state.title}
        </Dropdown.Item>
        {state.filter === "channels"
          ? channelNames.map((channelName, index) => (
              <Dropdown.Item
                key={index}
                eventKey={channelName}
                value={channelName}
              >
                {channelName}
              </Dropdown.Item>
            ))
          : availableDates.map((date, value) => (
              <Dropdown.Item key={date} eventKey={date}>
                {date}
              </Dropdown.Item>
            ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};

UseDropdown.PropTypes = {
  data: PropTypes.string,
};

export default UseDropdown;
