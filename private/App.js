/**
 * This file is the landing page for the Encoding Data Application
 *
 * Created by Zechariah Edens and Taani Maama on 03/14/2022
 * Last updated by Zechariah Edens on 03/14/2022
 * Copyright © 2022 Sinclair Broadcast Group - All rights Reserved
 */
import "./App.css";

function App() {
  return (
    <div className="App">
      <div className="Container">
        <h1 className="header">Encoding Data</h1>
        <div className="filters">
          <ui>
            <li></li>
            <li></li>
          </ui>
        </div>
        <div className="graph"></div>
      </div>
      <div clasName="footer"></div>
    </div>
  );
}

export default App;

// Channel
// Date of time
// Amount of transcoding being done
// future?
