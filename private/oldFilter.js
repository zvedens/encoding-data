import blobDataCondensed from "../utils/blobDataCondensed";
let blobDataConst = blobDataCondensed.programs;
let mapData = blobDataConst
  .filter((program) => {
    if (program.attributes.source.includes("GRACENOTE")) return program;
  })
  .map((program) => {
    const data = program.attributes;
    const transcoding = data.source;
    const startTime = new Date(data.startTime).toLocaleString("en-US", {
      timeZone: "PST",
    });
    const endTime = new Date(data.endTime).toLocaleString("en-US", {
      timeZone: "PST",
    });

    return {
      transcoding: transcoding,
      startTime: startTime,
      endTime: endTime,
    };
  });

// for each channelData(channel)
// for every interval,
// add 1 to our timeRange object for every matching time

for (let i = 0; i < channelData.length; i++) {
  const element = channelData[i];
  console.log(element);
}
const dataset = Object.values(timeRange);
const labels = Object.keys(timeRange);

// const labels = timeRange;
// const labels = _.range(1, 25);
